//
//  AddPhotoViewController.swift
//  PhotoCollection
//
//  Created by WS-Luthfi on 1/15/19.
//  Copyright © 2019 interfeis. All rights reserved.
//

import UIKit

class AddPhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var btnUpdate: UIButton!
    
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var imgPhoto: UIImageView!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    var imagePicker = UIImagePickerController();
    
    var picture : PhotoData? = nil;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self;
        
        if(picture != nil){
            print("we have a picture");
            
            imgPhoto.image = UIImage(data: picture?.photo_file as! Data);
            
            txtTitle.text = picture?.title;
            
            btnUpdate.setTitle("Update", for: .normal);
        }else{
            print("no, we dont have a picture");
            
            btnDelete.isHidden = true;
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnBarPhotoTapped(_ sender: Any) {
        
        imagePicker.sourceType = .photoLibrary;
        
        present(imagePicker, animated: true, completion: nil);
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage;
        
        imgPhoto.image = image;
        imagePicker.dismiss(animated: true, completion: nil);
    }

    @IBAction func btnBarCameraTapped(_ sender: Any) {
        imagePicker.sourceType = .camera;
        
        present(imagePicker, animated: true, completion: nil);
    }
    
    @IBAction func btnAddTapped(_ sender: Any) {
        
        if(picture != nil){
            picture!.title = txtTitle.text;
            picture!.photo_file = UIImagePNGRepresentation(imgPhoto.image!)! as NSData?;
        }else{
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext;
            
            let photo = PhotoData(context: context);
            
            photo.title = txtTitle.text;
            photo.photo_file = UIImagePNGRepresentation(imgPhoto.image!)! as NSData?;
        }
        
        
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext();
        
        navigationController!.popViewController(animated: true);
        
    }
    
    @IBAction func btnDeleteTapped(_ sender: Any) {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext;
        
        context.delete(picture!);
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext();
        
        navigationController!.popViewController(animated: true);
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
