//
//  TablePhotoViewController.swift
//  PhotoCollection
//
//  Created by WS-Luthfi on 1/14/19.
//  Copyright © 2019 interfeis. All rights reserved.
//

import UIKit

class TablePhotoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tvPhotoData: UITableView!
    
    var photos : [PhotoData] = [];

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tvPhotoData.dataSource = self;
        tvPhotoData.delegate = self;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let picture : PhotoData? = photos[indexPath.row];
        
        performSegue(withIdentifier: "photoDetailSegue", sender: picture);
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell();
        
        let photo : PhotoData = photos[indexPath.row];
        
        let txtRow : String = photo.title!;
        
        cell.textLabel?.text = txtRow;
        cell.imageView?.image = UIImage(data: photo.photo_file as! Data);
        
        return cell;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVC = segue.destination as! AddPhotoViewController;
        
        nextVC.picture = sender as? PhotoData;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPhotos();
        tvPhotoData.reloadData();
    }
    
    func getPhotos(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext;
        
        do{
            
            photos = try context.fetch(PhotoData.fetchRequest());
            
            print(photos);
            
        }catch{
            print("Error on fetching photos");
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

